#include <iostream>
#include <vector>
#include <string>
#include <cstdint>
#include <map>
// namespace HTML {

enum struct ATTRIBUTETYPE : uint8_t
{
	ID,
	CLASS,
	ATTRIBUTE_NAME,
	ATTRIBUTE_VALUE,
	END
};


struct attributeTokens
{
	ATTRIBUTETYPE type;
	std::string value;
};

class Node
{


public:

	enum NodeType
	{
		HTML,
		TAG,
		COMMENT,
	};


	Node();
	~Node();

	Node(const std::string& tag);
	Node(const NodeType& type, const std::string& tag, const std::string& content);
	Node(const NodeType& type, const std::string& tag, const std::string& attributes, const std::string& content);

	void set_tag_attributes(const std::string& attributes);
	void set_tag_content(const std::string& content);
	void append_child(const Node& child);

	std::string get_html_element();
private:

	void filter_attributes(const std::string& attributes);
	std::vector<attributeTokens> parse_attributes(const std::string& attributes);

	void add_to_attribute_types(std::vector<attributeTokens>& tokens, ATTRIBUTETYPE& type, const std::string& values);
	std::string filter_content(const std::string& content);

	bool isInline;

	std::string tag;
	std::string id;
	std::string element_content;


	std::map<std::string, std::string> attributes_values;

	std::vector<std::string> classes;

	std::vector<Node> children;



	NodeType node_type;

};

// }