#include "pch.h"
#include "Node.h"
#include <sstream>

Node::Node()
{

}

Node::~Node()
{

}

Node::Node(const std::string& tag) :
	node_type(Node::NodeType::TAG), tag(tag), element_content("")
{

}

Node::Node(const NodeType& type, const std::string& tag, const std::string& content) :
	node_type(type), tag(tag), element_content(content)
{

}


Node::Node(const Node::NodeType& type, const std::string& tag, const std::string& attributes, const std::string& content) :
	node_type(type), tag(tag), element_content(content)
{
	this->filter_attributes(attributes);
}

void Node::add_to_attribute_types(std::vector<attributeTokens>& tokens, ATTRIBUTETYPE& type, const std::string& values)
{	
	switch (type)
	{
	case ATTRIBUTETYPE::CLASS:
		tokens.push_back({ ATTRIBUTETYPE::CLASS, values });
		break;
	case ATTRIBUTETYPE::ID:
		tokens.push_back({ ATTRIBUTETYPE::ID, values });
		break;
	case ATTRIBUTETYPE::ATTRIBUTE_NAME:
		tokens.push_back({ ATTRIBUTETYPE::ATTRIBUTE_NAME, values });
		break;
	case ATTRIBUTETYPE::ATTRIBUTE_VALUE:
		tokens.push_back({ ATTRIBUTETYPE::ATTRIBUTE_VALUE, values });
		break;
	case ATTRIBUTETYPE::END:
		//return;
	default:
		break;
	}
}


std::vector<attributeTokens> Node::parse_attributes(const std::string& attributes)
{
	bool skip_next;
	std::vector<attributeTokens> tokens;
	std::string temp_store = "";
	ATTRIBUTETYPE attribute_type;
	if (this->node_type == Node::NodeType::TAG)
	{
		for (int i = 0; i < attributes.size(); i++)
		{
			if (attributes[i] == '\\') {
				std::cout << "scape " << attributes[i + 1] << std::endl;
			}
			if (attributes[i] == '.')
			{

				if (!temp_store.empty())
				{
					add_to_attribute_types(tokens, attribute_type, temp_store);
				}
				attribute_type = ATTRIBUTETYPE::CLASS;
				temp_store.clear();

				continue;
			}
			else if (attributes[i] == '#')
			{

				if (!temp_store.empty())
				{
					add_to_attribute_types(tokens, attribute_type, temp_store);
				}
				attribute_type = ATTRIBUTETYPE::ID;
				temp_store.clear();
				continue;
			}
			else if (attributes[i] == '[')
			{
				if (!temp_store.empty())
				{
					add_to_attribute_types(tokens, attribute_type, temp_store);
				}
				attribute_type = ATTRIBUTETYPE::ATTRIBUTE_NAME;
				temp_store.clear();
				continue;
			}
			else if (attributes[i] == '=' && attribute_type == ATTRIBUTETYPE::ATTRIBUTE_NAME)
			{
				if (!temp_store.empty())
				{
					add_to_attribute_types(tokens, attribute_type, temp_store);
				}
				attribute_type = ATTRIBUTETYPE::ATTRIBUTE_VALUE;
				temp_store.clear();
				continue;
			}
			else if (attributes[i] == ']' && (attribute_type == ATTRIBUTETYPE::ATTRIBUTE_NAME || attribute_type == ATTRIBUTETYPE::ATTRIBUTE_VALUE))
			{
				if (!temp_store.empty())
				{
					add_to_attribute_types(tokens, attribute_type, temp_store);
				}
				attribute_type = ATTRIBUTETYPE::END;
				temp_store.clear();
				continue;
			}

			temp_store += attributes[i];
		}
	}

	if (!temp_store.empty())
	{
		add_to_attribute_types(tokens, attribute_type, temp_store);
	}

	return tokens;
}


std::string Node::filter_content(const std::string& content)
{
	std::string temp;
	for (int i = 0; i < content.size(); i++)
	{
		if (content[i] == '<')
		{
			temp += "&amp;";
		}
		temp += content[i];
	}

	return content;
}

void Node::set_tag_content(const std::string& content)
{
	//element_content = filter_content(content);
	element_content = content;
}

void Node::set_tag_attributes(const std::string& attributes)
{
	this->filter_attributes(attributes);
}

void Node::append_child(const Node& child)
{
	children.push_back(child);
}

void Node::filter_attributes(const std::string& attributes)
{
	std::vector<attributeTokens> tokens = parse_attributes(attributes);


	std::cout << "vector cap " << tokens.size() << std::endl;
	for (int i = 0; i < tokens.size(); i++)
	{
		if (tokens[i].type == ATTRIBUTETYPE::CLASS) {
			classes.push_back(tokens[i].value);
		}

		if (tokens[i].type == ATTRIBUTETYPE::ID)
		{
			id = tokens[i].value;
		}

		if(tokens[i].type == ATTRIBUTETYPE::ATTRIBUTE_NAME)
		{
			std::string attribute_name = tokens[i].value;
			std::string attribute_value = "";
			if (i + 1 <= tokens.size()) {
				if (tokens[i + 1].type == ATTRIBUTETYPE::ATTRIBUTE_VALUE)
				{
					attribute_value = tokens[i + 1].value;
				}
			}
			attributes_values.insert({ attribute_name, attribute_value });
			
		}
	}
	std::cout << "content -> " << element_content << std::endl;

}


std::string Node::get_html_element()
{
	std::string final_val;

	std::string starting_tag;
	std::string closing_tag;

	if (Node::NodeType::TAG)
	{
		starting_tag = "<" + tag;
		final_val += starting_tag;
		if (!id.empty())
		{
			final_val += " id =";
			final_val += "\"";
			final_val += id;
			final_val += "\"";
		}

		if (classes.size() > 0)
		{
			final_val += " class = ";
			final_val += '\"';
			for (int i = 0; i < classes.size(); i++)
			{
				final_val += classes[i] + " ";
			}
			final_val += '\"';
		}

		if (!attributes_values.empty()) 
		{
			for (auto itr = attributes_values.begin(); itr != attributes_values.end(); ++itr) 
			{
				final_val += " ";
				final_val += itr->first;
				final_val += "=";
				final_val += "\"";
				final_val += itr->second;
				final_val += "\"";
				final_val += " ";
			}
		}

		final_val += ">";
		
		if (!element_content.empty())
		{
			final_val += element_content;
		}
		if (!children.empty()) {

			for (int i = 0; i < children.size(); i++)
			{
				final_val += children[i].get_html_element();
			}

		}

		

		closing_tag = "</" + tag + ">";
		final_val += closing_tag;
	}

	return final_val;
}

