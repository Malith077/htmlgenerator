// htmlGenerator.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "Node.h"
#include <fstream>

int main()
{
	Node element1("div");

	element1.set_tag_attributes(".class1.class2.class3[href=bvfbhvjbjfdh][src=vfdvdfvdf]");
	element1.set_tag_content("v bj jhbvhfdbuvhbfv");
	std::string file;

	std::ofstream out;

	for (int i = 0; i < 10; i++) {
		file += element1.get_html_element() + "\n";
	}

	Node div("div");
	Node p("p");
	p.set_tag_content("vbefhugbhfejsvhkjfsdv byfuhdjv bhfdj buyhfd bviyufbv iuyhefb uyvghefd bvuyd");

	div.append_child(p);

	std::cout << div.get_html_element() << std::endl;


	Node table("table");
	table.set_tag_attributes("[style = border: 1px solid black;]");

	for (int i = 0; i < 20; i++)
	{
		Node tr("tr");
		tr.set_tag_attributes("[style = border: 1px solid black;]");
		for (int j = 0; j < 20; j++)
		{
			Node td("td");
			td.set_tag_attributes("[style = border: 1px solid black;]");
			std::string data;
			data = "i = " + std::to_string(i) + "j = " + std::to_string(j);
			td.set_tag_content(data);
			tr.append_child(td);
		}
		table.append_child(tr);
	}
	
	std::string tableHTML = table.get_html_element();
	file += tableHTML;

	std::cout << file << std::endl;
	
	out.open("out.html");

	out << file;

	out.close();
	

    std::cout << "Hello World!\n"; 
}